module Reports
  class Detailed

  def self.content_row_finder

    subjects = Subject.sorted
    @combined = []

    subjects.each do |sub|
      if !sub.pages.present?
        line = sub.as_json.without("created_at", "updated_at").transform_keys{|key| "subject_"+key}
        zero_values_page = ["page_id", "page_name", "page_position", "page_visible", "page_permalink", "section_name", "section_id","section_content", "section_content_type","section_position", "section_visible", "section_topic"]
        zero_values_page.each do |x| line[x] = "" end
        @combined = @combined.push(line)
      else sub.pages.each do |p|
        if !p.sections.present?
          line = sub.as_json.without("created_at", "updated_at").transform_keys{|key| "subject_"+key}.merge(p.as_json.without("created_at", "updated_at", "subject_id").transform_keys{|key| "page_"+key})
          zero_values_section = ["section_name", "section_id","section_content", "section_content_type","section_position", "section_visible", "section_topic"]
          zero_values_section.each do |x| line[x] = "" end
          @combined = @combined.push(line)
        else
          p.sections.each do |sec|
            line = sub.as_json.without("created_at", "updated_at").transform_keys{|key| "subject_"+key}.merge(p.as_json.without("created_at", "updated_at", "subject_id").transform_keys{|key| "page_"+key}).merge(sec.as_json.without("created_at", "updated_at", "subject_id", "page_id").transform_keys{|key| "section_"+key})
            @combined = @combined.push(line)
          end
        end
      end
      end
    end

    @rows = @combined.sort_by{ |k| k["subject_id"]}
    return @rows

  end

  def self.summary_content
    sections = Section.all
    @sections_json = []

    sections.each do |x|
      section_item = {
          "section_name" => x.name,
          "section_id" => x.id,
          "section_position" => x.position,
          "section_topic" => x.topic,
          "section_content_type" => x.content_type,
          "page_name" => x.page.name,
          "page_id" => x.page.id,
          "page_permalink" => x.page.permalink,
          "page_position" => x.page.position,
          "page_visible" => x.page.visible,
          "subject_name" => x.page.subject.name,
          "subject_id" => x.page.subject.id,
      }
      @sections_json << section_item
    end
    return @sections_json
  end
end
end