class SummaryReportsController < ApplicationController

  layout "admin"

  before_action :confirm_logged_in
  require 'reports'

  def index
    @pages = Page.sorted
    @sections = Section.sorted
  end


  def detailed
    @combined = Reports::Detailed.content_row_finder

  end
end
