// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require turbolinks
//= require jquery
//= require_tree

$(document).ready(function () {
    $("#my_button").click(function AddPage() {

        $.ajax("access/random", {
            cache: false,
            dataType: "json",
            method: "get",
            error: errorHandler,
            success: success
        });

    })
    });

function success(page) {

    var new_line = '<li>%data%</li>';
    var generated_line = new_line.replace("%data%", page.name);
    $("#list").append(generated_line);
};



$(document).ready(function () {
    $("#summary_report_button").click(function AddSummaryReport() {

        $("#list1").removeClass("hidden");
        $.ajax("access/ajax_report", {
            cache: false,
            dataType: "json",
            method: "get",
            error: errorHandler,
            success: success_report
        });

    })
});

function success_report(line) {

    var data = line;
    for (var i = 0; i < data.length; i++) {
        $("#table_content").append("<tr>");
        $("#table_content").append("<td>" + data[i].section_name + "</td>");
        $("#table_content").append("<td>" + data[i].section_id + "</td>");
        $("#table_content").append("<td>" + data[i].section_position + "</td>");
        $("#table_content").append("<td>" + data[i].section_topic + "</td>");
        $("#table_content").append("<td>" + data[i].section_content_type + "</td>");
        $("#table_content").append("<td>" + data[i].section_visible + "</td>");
        $("#table_content").append("<td>" + data[i].page_name + "</td>");
        $("#table_content").append("<td>" + data[i].page_id + "</td>");
        $("#table_content").append("<td>" + data[i].page_permalink + "</td>");
        $("#table_content").append("<td>" + data[i].page_position + "</td>");
        $("#table_content").append("<td>" + data[i].page_visible + "</td>");
        $("#table_content").append("<td>" + data[i].subject_name + "</td>");
        $("#table_content").append("<td>" + data[i].subject_id + "</td>");
        $("#table_content").append("</tr>");
    }
}

$(document).ready(function () {
    $("#detailed_report_button").click(function AddDetailedReport() {

        $("#list2").removeClass("hidden");
        $.ajax("access/detailed_report", {
            cache: false,
            dataType: "json",
            method: "get",
            error: errorHandler,
            success: detailed_report

        });

    })
});
function detailed_report(line) {

    var data = line;
    for (var i = 0; i < data.length; i++) {
        $("#table_content_detailed").append("<tr>");
        $("#table_content_detailed").append("<td>" + data[i].subject_id + "</td>");
        $("#table_content_detailed").append("<td>" + data[i].subject_name + "</td>");
        $("#table_content_detailed").append("<td>" + data[i].subject_position + "</td>");
        $("#table_content_detailed").append("<td>" + data[i].subject_visible + "</td>");
        $("#table_content_detailed").append("<td>" + data[i].page_id + "</td>");
        $("#table_content_detailed").append("<td>" + data[i].page_name + "</td>");
        $("#table_content_detailed").append("<td>" + data[i].page_permalink + "</td>");
        $("#table_content_detailed").append("<td>" + data[i].page_position + "</td>");
        $("#table_content_detailed").append("<td>" + data[i].page_visible + "</td>");
        $("#table_content_detailed").append("<td>" + data[i].section_name + "</td>");
        $("#table_content_detailed").append("<td>" + data[i].section_id + "</td>");
        $("#table_content_detailed").append("<td>" + data[i].section_position + "</td>");
        $("#table_content_detailed").append("<td>" + data[i].section_topic + "</td>");
        $("#table_content_detailed").append("<td>" + data[i].section_content_type + "</td>");
        $("#table_content_detailed").append("<td>" + data[i].section_visible + "</td>");
        $("#table_content_detailed").append("</tr>");
    }
}



function errorHandler() {
    alert("Error");
}

