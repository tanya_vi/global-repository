Rails.application.routes.draw do


  root "access#index"

  get 'show/:permalink', :to => 'public#show'

  resources :subjects

  get 'summary_reports/index', :to => "summary_reports#index"
  get 'summary_reports/detailed', :to => "summary_reports#detailed"

  #get 'access/index'

  get 'admin', :to => "access#index"

  match ':controller(/:action(/:id))', :via => [:get, :post]

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
