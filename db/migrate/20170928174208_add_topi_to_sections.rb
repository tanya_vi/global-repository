class AddTopiToSections < ActiveRecord::Migration[5.1]
  def change
    add_column :sections, :topic, :string
  end
end
