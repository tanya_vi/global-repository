class AddSubjectIdToSections < ActiveRecord::Migration[5.1]
  def up
    add_column :sections, :subject_id, :integer
  end
  def down
    add_column :sections, :subject_id
  end

end
